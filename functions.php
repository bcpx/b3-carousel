<?php 

function bootstrap_carousel_jquery()
{
	// Register bootstrap.min.js:
	wp_register_script( 'custom-script1', get_template_directory_uri() . '/dist/js/bootstrap.min.js', array( 'jquery' ) );
	//  Enqueue bootstrap.min.js:
	wp_enqueue_script( 'custom-script1' );
	
	// Register holder.js:
	wp_register_script( 'custom-script2', get_template_directory_uri() . '/dist/js/holder.js', array( 'jquery') );
	// Enqueue holder.js:
	wp_enqueue_script( 'custom-script2' );			
}
add_action( 'wp_enqueue_scripts', 'bootstrap_carousel_jquery' );

if ( function_exists('register_sidebar') )
	register_sidebar(array(
		'before_widget' => '',
		'after_widget' => '',
		'before_title' => '<h3>',
		'after_title' => '</h3>',
	));

remove_filter( 'the_content', 'wpautop' );
remove_filter( 'the_excerpt', 'wpautop' );

?>